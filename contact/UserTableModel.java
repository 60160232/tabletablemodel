/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contact;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author HP
 */
public class UserTableModel extends AbstractTableModel{

    String[] columName = {"id","username","name","surname"};
    ArrayList<user> userList = Data.userList;
    @Override
    public String getColumnName(int column) {
        return columName[column];
    }

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        user User = userList.get(rowIndex);
        if(User == null)return "";
        switch(columnIndex){
            case 0: return User.getId();
            case 1: return User.getLogin();
            case 2: return User.getName();
            case 3: return User.getSurname();
        }
        return "";
    }
    
}
